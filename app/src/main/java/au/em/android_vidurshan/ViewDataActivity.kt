package au.em.android_vidurshan

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.Toast
import au.em.android_vidurshan.database.User
import au.em.android_vidurshan.database.UserDatabase
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_view_data.*
import kotlinx.android.synthetic.main.user_ticket.view.*
import kotlinx.coroutines.launch


class ViewDataActivity : BaseActivity() {

    var listOfUser = ArrayList<User>()
    var adapter: UserAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_data)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

//load all data list view from room database
        launch {
            applicationContext?.let {
                var listOfUser2 = UserDatabase(it).getUserDao().loadall()
                adapter = UserAdapter(it, listOfUser2 as ArrayList<User>)
                listViewUsers.adapter = adapter
            }
        }



    }


    //display data to list view
    inner class UserAdapter : BaseAdapter {
        var listOfUser = ArrayList<User>()
        var context: Context? = null

        constructor(context: Context, listOfUser: ArrayList<User>) : super() {
            this.listOfUser= listOfUser
            this.context = context
        }

        override fun getCount(): Int {
            return listOfUser.size
        }

        override fun getItem(position: Int): Any {
            return listOfUser[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val user = listOfUser[position]
            val inflater =
                context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            var myview = inflater.inflate(R.layout.user_ticket, null)
            myview.textViewUsername.text = user.userName
            myview.textViewFirstname.text = user.firstName
            myview.textViewLastName.text = user.lastName
            myview.textViewEmail.text = user.email
            //if long press to delete
            myview.setOnLongClickListener(){
                popUpToDelete(user)
                return@setOnLongClickListener true
            }
            return myview
        }

        //delete data frm database and memory
        fun deleteData(user:User) {
            launch {
                applicationContext?.let {
                    UserDatabase(it).getUserDao().deleteByUserName(user)
                    Toast.makeText(it, "data deleted", Toast.LENGTH_LONG).show()
                    listOfUser.remove(user)
                    adapter!!.notifyDataSetChanged()
                }
            }
        }
        //clas alert when delete data
        fun popUpToDelete(user:User) {

            val builder = AlertDialog.Builder(this@ViewDataActivity)
            //set title for alert dialog
            builder.setTitle(R.string.alert_title)
            //set message for alert dialog
            builder.setMessage(R.string.alert_supporting_text)
            builder.setIcon(android.R.drawable.ic_dialog_alert)

            //performing positive action
            builder.setPositiveButton("Yes"){dialogInterface, which ->
                deleteData(user)
                           }
            //performing cancel action
            builder.setNeutralButton("Cancel"){dialogInterface , which ->
                dialogInterface.dismiss()
            }
            //performing negative action
            builder.setNegativeButton("No"){dialogInterface, which ->
                dialogInterface.dismiss()
            }
            // Create the AlertDialog
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }
}


