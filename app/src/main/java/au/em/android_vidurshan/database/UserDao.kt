package au.em.android_vidurshan.database

import androidx.room.*


@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(vararg user: User)

    @Query("SELECT * FROM user  ")
    suspend fun loadall():List<User>

    @Query("SELECT email FROM user WHERE  userName=(:username) ")
    suspend fun checkUsername(username: String):String

    @Delete
    suspend fun deleteByUserName(user: User)

}