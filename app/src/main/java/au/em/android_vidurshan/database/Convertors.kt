package au.em.android_vidurshan.database

import androidx.room.TypeConverter
import com.google.gson.Gson

class Convertors {
    @TypeConverter
    fun listToJson(value: List<User>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<User>::class.java).toList()
}