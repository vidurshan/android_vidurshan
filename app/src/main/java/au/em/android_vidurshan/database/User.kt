package au.em.android_vidurshan.database



import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey val userName:String,
    val firstName:String,
    val lastName:String,
    val email:String,

)

