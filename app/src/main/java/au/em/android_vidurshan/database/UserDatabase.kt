package au.em.android_vidurshan.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = arrayOf(User::class),version =1)
@TypeConverters(Convertors::class)
abstract class UserDatabase :RoomDatabase(){

    abstract fun getUserDao(): UserDao

    companion object {

        @Volatile
        private var instance: UserDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDataBase(context).also {
                instance = it
            }
        }


        private fun buildDataBase(context: Context) = Room.databaseBuilder(
            context.applicationContext, UserDatabase::class.java,"userDatabase").build()
    }
}