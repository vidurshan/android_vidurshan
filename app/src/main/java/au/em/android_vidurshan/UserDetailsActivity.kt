package au.em.android_vidurshan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import au.em.android_vidurshan.database.User
import au.em.android_vidurshan.database.UserDatabase
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_user_details.*
import kotlinx.coroutines.launch

class UserDetailsActivity : BaseActivity() {

    var firstName:String? = null
    var lastName:String? = null
    var userName:String? = null
    var email:String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_user_details)
        //disable keyboard on launch
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

      //move to view data when buttonViewData is clicked
        buttonViewData.setOnClickListener {
            var intent = Intent(this,ViewDataActivity::class.java)
            startActivity(intent)
        }

        //save to room databse when save buitton is clicked
        buttonSave.setOnClickListener {
            firstName =  editTextFirstName.text.toString()
            lastName = editTextLastname.text.toString()
            userName =  editTextUsername.text.toString()
            email =  editTextUsername.text.toString()
            //validate before save data
            if(validateFirstName()&&  validateLastName() &&validateUserName() &&validateEmailEmpty()){
                launch {
                    val user =User(userName!!,firstName!!,lastName!!,email!!)
                    applicationContext?.let {
                        UserDatabase(it).getUserDao().insertAll(user)
                        Toast.makeText(it ,"success",Toast.LENGTH_LONG).show()
                        clearfield()
                    }
                }
            }
            else{
                //if validation fails
                return@setOnClickListener
            }
        }

    //runtime email format validation
        editTextEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?){}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int){}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int){
                if(android.util.Patterns.EMAIL_ADDRESS.matcher(editTextEmail.text.toString()).matches()){
                    correctEmail()
                } else {
                    wrongEmail()
                }
            }
        })
    }

    //check for first name is empty
    fun validateFirstName():Boolean{
        if (!firstName.equals("")){
            textViewErrorFirstName.visibility= View.GONE
            editTextFirstName.setBackgroundResource(R.drawable.edittext_normal_border)
            return true

        } else{
           textViewErrorFirstName.visibility=View.VISIBLE
            editTextFirstName.setBackgroundResource(R.drawable.edittext_border_red_error)
            return false
        }
    }

    //check for last name is empty
    fun validateLastName():Boolean{
        if (!lastName.equals("")){
            textViewErrorLastName.visibility= View.GONE
            editTextLastname.setBackgroundResource(R.drawable.edittext_normal_border)
            return true

        } else{
            textViewErrorLastName.visibility=View.VISIBLE
            editTextLastname.setBackgroundResource(R.drawable.edittext_border_red_error)
            return false
        }
    }
    //check for user name is empty
    fun validateUserName():Boolean{
        if (!userName.equals("")){
           textViewErrorUserName.visibility= View.GONE
            editTextUsername.setBackgroundResource(R.drawable.edittext_normal_border)
            return true

                  } else{
            textViewErrorUserName.visibility=View.VISIBLE
            editTextUsername.setBackgroundResource(R.drawable.edittext_border_red_error)
            return false
        }
    }
    //check for email is empty
    fun validateEmailEmpty():Boolean{
        if (!email.equals("")){
            textViewErrorEmail.visibility= View.GONE
            editTextEmail.setBackgroundResource(R.drawable.edittext_normal_border)
            return true

        } else{
            textViewErrorEmail.visibility=View.VISIBLE
            editTextEmail.setBackgroundResource(R.drawable.edittext_border_red_error)
            return false
        }
    }


    //to dispaly error when validating email
    fun correctEmail(){
        textViewErrorEmail.visibility = View.GONE
        editTextEmail.setBackgroundResource(R.drawable.edittext_normal_border)
        email = editTextEmail.text.toString()
    }


    fun wrongEmail() {
        textViewErrorEmail.visibility= View.VISIBLE
        editTextEmail.setBackgroundResource(R.drawable.edittext_border_red_error)
    }



    //make all edittext empty
    fun clearfield(){
        editTextFirstName.setText("")
        editTextLastname.setText("")
        editTextUsername.setText("")
        editTextEmail.setText("")
    }





}